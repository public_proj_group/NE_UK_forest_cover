# NE England Forest Cover maps



## Description

### Overview

This project will uncover how forest cover in the NE of England changed in the aftermath of Storm Arwen. It will do this by updating the UKCEH land cover map which currently only extends to 2021, the year before Arwen hit. 

### Outline

#### Sentinel 2 data
- Average S2 data is computed quarterly. 
- Uses Google Earth Engine to do this. 
- Clouds/shadows are removed. 
- The surface relectance is computed using the median value for each pixel over the quarter year. 
- Use 12 of the available S2 bands, as per UKCEH. 


#### Land cover map

- The original UKCEH land cover map used 4 quarter year average reflectance maps from Sentinel 2 to create an annual average land cover map. 
- Used a random forest classifier trained on a number of labelled sub-grids from the whole of the UK.  
- Here the map will be updated for each quarter, to allow for more regular updates, therefore we'll need to construct 4 mappings from S2 to land cover, one for each season. This is a harder problem than the original, broad leaf trees change throughout the year and look very much like fields in winter. Focus is on the NE of England, area hit hardest by Arwen. 


## Tech

*xarray* to input tiffs [easier to manipulate than a rasterio tiff read]

*scikit_learn* for ML [ better than tensorflow for the random forest classifier]

*joblib* parallelise where possible 

### Original
The UKCEH map used a random forest classifier. 

### ML
We'll start with random forest, this was what was originally used. 


## Code
Don't forget to run:
export PYTHONPATH="${PYTHONPATH}:$HOME/NE_UK_forest_cover/src"

- src:  functions to run the ML, testing, plotting.
- notebooks: some notebooks that do a bit of testing.
- data: not on git, the tiffs we analyse.
- explore_datasets: scripts to take a look at the tiffs.  
- earth_engine: the code to get the S2 data from Google Earth Engine 

## Results

The original map looks like this:

![orig map](ukceh_cover.png)

Using an S2 image for Jan-Mar 2020 which looks like this:

![s2](jm_2020_rgb.png)

yields a new map, which looks like this:

![new map](jm_2020_cover.png)

This used a random forest with 250 trees with a max depth of 200. It used 60% of the system memory. 
Although there is a fair amount of mismatch, in this area the new map is actually a better classifier of the woodland types than the UKCEH version. (Local knowledge here)


## Limitations
This is designed to run on a memory inhibited weedy laptop. Code has been optimised for this. 
- can't train on the complete dataset (only about 10% at a time)
- random forest very memory intensive, limited to few shallow trees. 
- 




## Project status
Live as of Oct 2023