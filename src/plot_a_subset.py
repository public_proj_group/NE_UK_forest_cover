import rasterio
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
from matplotlib import colormaps
from CEH_land_cover_colours import ceh_cmap


def plot_tiff_s2(tiff):
    """
    plot_tiff_s2(tiff)

    input: tiff an xarray dataset read in using rasterio

    This plots RGB channels of a Sentinel 2 tiff over 20km x 20km near Todburn
    """

    x1 = 410820
    x2 = 412820

    y1 = 596472
    y2 = 594472

    tiff = tiff.sel(x=slice(x1, x2), y=slice(y1, y2), band=[2, 3, 4])

    # get the visible bands
    reds = tiff.band_data[2, :, :]  # [:100,:100]
    blues = tiff.band_data[0, :, :]  # [:100,:100]
    greens = tiff.band_data[1, :, :]  # [:100,:100]

    # get an RGB vector for plotting.
    rgb = np.dstack((reds, greens, blues))

    # normalise the data for plotting
    rgb /= 10000  # s2_sr values range from 0-10,000
    rgb = np.clip(rgb, 0, 0.25)  # the range of reflectance should sit between 0 and .25
    rgb /= 0.25  # get the values to be 0-1 for plotting

    plt.figure()
    x_plt = np.linspace(tiff.x[0], tiff.x[-1], 200)
    y_plt = np.linspace(tiff.y[1], tiff.y[-1], 200)

    plt.pcolormesh(x_plt[:], y_plt[:], rgb[:, :])


def plot_tiff_ukceh(tiff):
    """
    plot_tiff_ukceh(tiff)

    input: tiff an xarray dataset read in using rasterio

    This plots the classificaitons of a UKCEH cover map over 20km x 20km near Todburn
    """

    x1 = 410820
    x2 = 412820

    y1 = 596472
    y2 = 594472

    tiff = tiff.sel(x=slice(x1, x2), y=slice(y1, y2))
    tiff = tiff.band_data

    plt.figure()
    x_plt = np.linspace(tiff.x[0], tiff.x[-1], 200)
    y_plt = np.linspace(tiff.y[1], tiff.y[-1], 200)

    new_cmap = ceh_cmap()
    plt.pcolormesh(
        x_plt[:], y_plt[:], tiff[0, :, :], cmap=new_cmap, vmax=21.5, vmin=0.5
    )
    plt.colorbar()


def plot_todburn():
    """
    a wrapper function to plot predefined maps of UKCEH and S2
    """

    s2 = xr.open_dataset(
        "data/2020_Jan_Mar-0000000000-0000000000.tif", engine="rasterio"
    )

    plot_tiff_s2(s2)
    plt.title("RGB Jan-Mar 2020 S2 data")
    plt.savefig("jm_2020_rgb.png")

    ukceh_data = xr.open_dataset(
        "data/ukregion-northeastengland.tif", engine="rasterio"
    )

    plot_tiff_ukceh(ukceh_data)
    plt.title("Land cover from UKCEH")
    plt.savefig("ukceh_cover.png")
