import xarray as xr
from dataset_rejigging_funcs import *
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from sklearn.ensemble import RandomForestClassifier
from plot_a_subset import *

plt.ion()


# ====================

# having tuned the model we can now run it on actual data and plot the output

# ====================


# Read in the datasets, NB xr.open_dataset is a pointer to the data
# get the subsets:
# UKCEH - area
# S2 bands 2-8, 11, 12
# TODO -  start with 1 season but move on to all 4

ukceh_data = xr.open_dataset("data/ukregion-northeastengland.tif", engine="rasterio")
s2_data = xr.open_dataset(
    "data/2020_Jan_Mar-0000000000-0000000000.tif", engine="rasterio"
)

# ============================

# get the edge co-ords of the grids of the 2 datasets.
ukceh_corners = get_corners(ukceh_data)
s2_corners = get_corners(s2_data)

print("got data")

# ==========

# flatten the datasets

s2_dims, s2_flat = select_bands_areas(
    s2_data, s2_corners, [2, 3, 4, 5, 6, 7, 8, 11, 12]
)
ukceh_dims, ukceh_flat = select_bands_areas(ukceh_data, s2_corners, [1])


# DESTRUCTIVE !!
# remove the nan from the data, now the dims are nonsense.

s2_flat = np.squeeze(s2_flat[:, np.argwhere(np.isnan(ukceh_flat)[0, :] == False)])
ukceh_flat = ukceh_flat[(np.isnan(ukceh_flat) == False)]  # ukceh_flat[ukceh_flat>0]
ukceh_flat = ukceh_flat.astype("int8")

# TODO
# there are still nan in the S2 data.
# quick hack to remove we'll just set values to 0, so much data this won't affect the training.

s2_flat[np.isnan(s2_flat)] = 0.0

print("flattened")


# ================
# train test/split

# no need to do this as we are now using all the 10 step data to make the model
# we'll test on a subset.

X_train = s2_flat.T
y_train = ukceh_flat

# ================
# set up the model

clf = RandomForestClassifier(max_depth=200, random_state=0, n_estimators=250)


with parallel_backend("threading", n_jobs=4):
    # ===============
    # train
    clf.fit(X_train, y_train)

    # ================
    # score
    print(clf.score(X_train, y_train))


# a quick test to see how well this performs

corners_quick_check = [410820, 594472, 412820, 596472]

s2_dims, s2_flat = select_bands_areas(
    s2_data, corners_quick_check, [2, 3, 4, 5, 6, 7, 8, 11, 12], subset_step=1
)


y_pred = clf.predict(s2_flat.T)

predicted_cover_map = np.reshape(y_pred, (s2_dims[1], s2_dims[2]))

plt.figure()
x_plt = np.linspace(corners_quick_check[0], corners_quick_check[2], 200)
y_plt = np.linspace(corners_quick_check[3], corners_quick_check[1], 200)

new_cmap = ceh_cmap()
plt.pcolormesh(
    x_plt[:], y_plt[:], predicted_cover_map, cmap=new_cmap, vmax=21.5, vmin=0.5
)
plt.colorbar()

plt.title("Land cover from Jan-Mar 2020 S2 data")
plt.savefig("jm_2020_cover.png")
