from matplotlib.colors import ListedColormap
from numpy import array


def ceh_cmap():
    """
    cmap = ceh_cmap()

    a listed colourmap of the colours that UKCEH suggests for it's land cover map
    """

    cmap = [
        (255, 0, 0),
        (0, 102, 0),
        (115, 38, 0),
        (0, 255, 0),
        (127, 229, 127),
        (112, 168, 0),
        (153, 129, 0),
        (255, 255, 0),
        (128, 26, 128),
        (230, 140, 166),
        (0, 128, 115),
        (210, 210, 255),
        (0, 0, 128),
        (0, 0, 255),
        (204, 179, 0),
        (204, 179, 0),
        (255, 255, 128),
        (255, 255, 128),
        (128, 128, 255),
        (0, 0, 0),
        (128, 128, 128),
    ]

    cmap = array(cmap) / 255

    cmap = ListedColormap(cmap)

    return cmap
