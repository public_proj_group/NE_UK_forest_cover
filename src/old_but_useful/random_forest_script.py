# here we'll set up a random forest classifier on our subsets of data
import tensorflow_decision_forests as tfdf
import xarray as xr
import numpy as np
import tensorflow as tf
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix,ConfusionMatrixDisplay
from joblib import parallel_backend

#====================
# DEPRECATED

# an all in one script to read in the data and test a random forest classifier.

#====================


# Read in the datasets, NB xr.open_dataset is a pointer to the data
# get the subsets:
# UKCEH - area
# S2 bands 2-8, 11, 12
# TODO -  start with 1 season but move on to all 4 

ukceh_data = xr.open_dataset("data/ukregion-northeastengland.tif",engine="rasterio")

s2_data = xr.open_dataset("data/2020_Jan_Mar-0000000000-0000000000.tif",engine="rasterio")

print ('got data')


#==================================

# find the bounds of the 2 datasets. 
# TODO - this subsetting routine could be sped up by using gdal to do it once and create new tiffs that match up. 

def get_corners(input_ds):
    # replicate rasterio's .bounds
    # corners = ( left, bottom, right, top)
    # tiffs go from bottom to top so need to reverse the .y dimension

    return (input_ds.x.data[0], input_ds.y.data[-1], input_ds.x.data[-1], input_ds.y.data[0])

# get the edge co-ords of the grids of the 2 datasets. 
ukceh_corners = get_corners(ukceh_data)
s2_corners = get_corners(s2_data)


#==================
# prepare data for ingestion to tensorflow

# a function to create 2d data from 3d tiff
def flatten_tiff(inp,dim_keep=0):
    # inp_dims,reshaped_inp = flatten_tiff(inp,dim_keep=0)

    # input: xr data_array
    # output np.array
    
    # flattens an input data array from 3d to 2d
    # works if we want to squash the final dims and keep the first
    # 
    
    inp_dims = inp.shape
    if (len(inp_dims)) == 2:
        inp = np.expand_dims(inp,axis=0)
        inp_dims = inp.shape
    if (dim_keep == 0):
        reshaped_inp = (np.reshape(inp,(inp_dims[0],inp_dims[1]*inp_dims[2])) )
        
        
    return inp_dims,reshaped_inp


def select_bands_areas(dataset,corners,bands):

    #  ds_dims, ds_flat = select_bands_areas(dataset,corners,bands)

    subset_step = 10 # use this for making dataset manageable for testing. 
    ds_subset = dataset.sel(x=slice(corners[0],corners[2],subset_step),y=slice(corners[3],corners[1],subset_step),band=bands)

    ds_dims,ds_flat = flatten_tiff(ds_subset.band_data.data)

    return ds_dims, ds_flat

# # get subsets

# subset_step = 10 # use this for making dataset manageable for testing. 
# ukceh_subset = ukceh_data.sel(x=slice(s2_corners[0],s2_corners[2],subset_step),y=slice(s2_corners[3],s2_corners[1],subset_step),band=1)
# s2_subset = s2_data.sel(x=slice(s2_corners[0],s2_corners[2],subset_step),y=slice(s2_corners[3],s2_corners[1],subset_step),band=[2,3,4,5,6,7,8,11,12])


# # s2_subset = s2_data.sel(band=[2,3,4,5,6,7,8,11,12]) # use this for all the data



# print('got subsets')


# s2_dims,s2_flat = flatten_tiff(s2_subset.band_data.data)
# ukceh_dims, ukceh_flat = flatten_tiff(ukceh_subset.band_data.data)

s2_dims,s2_flat = select_bands_areas(s2_data,s2_corners,[2,3,4,5,6,7,8,11,12])
ukceh_dims, ukceh_flat = select_bands_areas(ukceh_data,s2_corners,[1])


# DESTRUCTIVE !!
# remove the nan from the data, now the dims are nonsense.

s2_flat = np.squeeze(s2_flat[:,np.argwhere(np.isnan(ukceh_flat)[0,:]==False)])
ukceh_flat = ukceh_flat[(np.isnan(ukceh_flat)==False)] # ukceh_flat[ukceh_flat>0]
ukceh_flat = ukceh_flat.astype("int8")

# TODO
# there are still nan in the S2 data.
# quick hack to remove we'll just set values to 0, so much data this won't affect the training.

s2_flat[np.isnan(s2_flat)] = 0.


print('flattened')

#====================
# set up the classes as classifiers,
# 1 is broad leaved, 2 is coniferous



#================
# train test/split

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(s2_flat.T, ukceh_flat, test_size=0.33, random_state=42)


#================
# set up the model
from sklearn.ensemble import RandomForestClassifier
clf = RandomForestClassifier(max_depth=10, random_state=0,n_estimators=100)

# model = tfdf.keras.RandomForestModel()
# model.fit(train_ds)

# Summary of the model structure.
# model.summary()

# Evaluate the model.
# model.evaluate(test_ds)


with parallel_backend('threading', n_jobs=4):

    #===============
    # train
    clf.fit(X_train,y_train)


    #================
    # validate
    clf.score(X_test,y_test)
    y_pred = clf.predict(X_test)


plt.ion()
n,bins = np.histogram(y_pred[y_test==2],np.arange(0.5,21))
plt.stairs(n/sum(n),bins,fill=True)
plt.xticks(range(1,21))

plt.figure()
plt.pcolormesh(confusion_matrix(y_test,y_pred,normalize='true'))
plt.ylim(20,0)
plt.xlabel('predictions')
plt.ylabel('true')
plt.set_cmap('Reds')
plt.title(clf.score(X_test,y_test))
