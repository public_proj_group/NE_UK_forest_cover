from joblib import parallel_backend
from woodland_tuning_plots import woods_accuracy


def run_model(clf, X_train, y_train, X_test, y_test):
    """
    returns:
    y_pred, the predictions made on the test set.

    runs the fit/evaluate on the classifier using 4 cores.

    """

    with parallel_backend("threading", n_jobs=4):
        # ===============
        # train
        clf.fit(X_train, y_train)

        # ================
        # validate
        print("test set accuracy %.4f" % (clf.score(X_test, y_test)))
        y_pred = clf.predict(X_test)

        print(
            "correctly classified woodland %.4f" % (woods_accuracy(y_pred, y_test)[0])
        )
        print(
            "incorrectly classified as woodland %.4f"
            % (woods_accuracy(y_pred, y_test)[1])
        )

        return y_pred
