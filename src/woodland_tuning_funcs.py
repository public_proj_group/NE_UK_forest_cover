import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay

"""
A selection of functions to produce woodland specific metrics

woodland_plots : bars of what woodland is or isn't.
misclassified: normalised counts of misclassified categories.
misclassified_as: normalised counts of what a misclassified category is classified as.
woods_accuracy: an accuracy type score for combined woodland types. 
miscat_plots: bars of the misclassified counts.
confusion_plots: what it says on the tin. 

"""


def woodland_plots(y_pred, y_test, no_of_categories=21):
    # bar plots for the 2 woodland types

    plt.figure()
    plt.subplot(121)
    n, bins = np.histogram(y_pred[y_test == 1], np.arange(0.5, no_of_categories))
    plt.stairs(n / sum(n), bins, fill=True)
    plt.xticks(range(1, no_of_categories))
    plt.ylim(0, 0.8)
    plt.title("what are type 1 broadleaved \n woods predicted to be")

    plt.subplot(122)
    n, bins = np.histogram(y_pred[y_test == 2], np.arange(0.5, no_of_categories))
    plt.stairs(n / sum(n), bins, fill=True)
    plt.xticks(range(1, no_of_categories))
    plt.title("what are type 2 coniferous \n woods predicted to be")
    plt.ylim(0, 0.8)

    plt.figure()
    plt.subplot(121)
    n, bins = np.histogram(y_test[y_pred == 1], np.arange(0.5, no_of_categories))
    plt.stairs(n / sum(n), bins, fill=True)
    plt.xticks(range(1, no_of_categories))
    plt.ylim(0, 0.9)
    plt.title("what are predicted type 1 \n broadleaved woods actually")

    plt.subplot(122)
    n, bins = np.histogram(y_test[y_pred == 2], np.arange(0.5, no_of_categories))
    plt.stairs(n / sum(n), bins, fill=True)
    plt.xticks(range(1, no_of_categories))
    plt.title("what are predicted type 2 \n coniferous woods actually ")
    plt.ylim(0, 0.9)


def misclassified(y_pred, y_test, class_no):
    """
    How many things that are not class_no are incorrectly categorised as being class_no?
    This is the fraction of class_no categorisations that are incorrect.

    Of the values in test set that are not class_no, how many are categorised as class_no
    normalised by the total number of predictions that are categorised as class_no

    """
    junk = np.sum(y_pred[y_test != class_no] == class_no) / np.sum(
        y_pred[y_pred == class_no]
    )
    return junk


def misclassified_as(y_pred, y_test, class_no_2, mis_class_as):
    """
    Is something that is a broadleaf mis categorised as conifer as a fraction of the actual broad leafs?

    Of the values in test set that are class_no_2, how many are miscategorised as mis_class_as
    normalised by the total number of test that are actually class_no_2

    How many broad leaf trees are miscategorised as coniferous, out of all of the broad leaf trees

    """
    junk = np.sum(y_pred[y_test == class_no_2] == mis_class_as) / np.sum(
        y_test[y_test == class_no_2]
    )
    if class_no_2 == mis_class_as:
        junk = 0.0
    return junk


def woods_accuracy(y_pred, y_test):
    """
    We really want to know if woods are correctly identiified as woods. don't care if its braod leaf or confier

    returns:
    (1)  fraction of actual woodland cover that is correctly identified as wood.
    (2)  fraction of the woodland cover predictions that are not actually woodland


    """

    #  counts where woodland of any type is correctly identified
    correct_pred_count = np.sum(y_pred[y_test == 1] == 1)  # 1 == 1
    correct_pred_count += np.sum(y_pred[y_test == 2] == 2)  # 2 == 2
    correct_pred_count += np.sum(y_pred[y_test == 1] == 2)  # 1 == 2
    correct_pred_count += np.sum(y_pred[y_test == 2] == 1)  # 2 == 1

    # count of actual woodland cover
    forest_count = np.sum(y_test == 1)
    forest_count += np.sum(y_test == 2)

    # counts where woodland is incorrectly predicted
    # any cat > 2 is not forest so use these
    incorrect_pred_count = np.sum(y_pred[y_test > 2] == 1)  # not forest = 1
    incorrect_pred_count += np.sum(y_pred[y_test > 2] == 2)  # not forest = 2
    # incorrect_pred_count += np.sum(y_pred[y_test!=1]==2) # 1 != 2
    # incorrect_pred_count += np.sum(y_pred[y_test!=2]==1) # 2 != 1

    # count of predicted woodland cover
    forest_pred_count = np.sum(y_pred == 1)
    forest_pred_count += np.sum(y_pred == 2)

    return correct_pred_count / forest_count, incorrect_pred_count / forest_pred_count


def miscat_plots(y_pred, y_test, no_of_categories=21):
    plt.figure()
    plt.subplot(121)
    plt.stairs(
        [misclassified_as(y_pred, y_test, 1, i) for i in range(1, no_of_categories)],
        np.arange(0.5, no_of_categories),
        fill="true",
    )
    plt.ylim(0, 0.15)
    plt.title("broadleaf (1) \n misclassified as ...")
    plt.xticks(range(1, no_of_categories))
    plt.axvline(1, color="r")

    plt.subplot(122)
    plt.stairs(
        [misclassified_as(y_pred, y_test, 2, i) for i in range(1, no_of_categories)],
        np.arange(0.5, no_of_categories),
        fill="true",
    )
    plt.ylim(0, 0.15)
    plt.title("confierous (2)  \n misclassified as ...")
    plt.xticks(range(1, no_of_categories))
    plt.axvline(2, color="r")


def confusion_plots(y_pred, y_test):
    conf_mat = confusion_matrix(y_test, y_pred, normalize="true")

    plt.figure()
    plt.subplot(121)
    plt.pcolormesh(conf_mat)
    plt.ylim(20, 0)
    plt.xlabel("predictions")
    plt.ylabel("true")
    plt.set_cmap("Reds")
    # plt.title(clf.score(X_test,y_test))

    plt.subplot(122)
    plt.pcolormesh(conf_mat[:5, :5])
    plt.ylim(4, 0)
