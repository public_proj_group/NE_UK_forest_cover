import tensorflow_decision_forests as tfdf
import xarray as xr
import numpy as np
import tensorflow as tf
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from joblib import parallel_backend

# A suite of functions to prepare data for manipulation


# ==================================

# find the bounds of the 2 datasets.
# TODO - this subsetting routine could be sped up by using gdal to do it once and create new tiffs that match up.


def get_corners(input_ds):
    """
    corners = get_corners(input_ds)

    input: xarray dataset read in using rasterio
    ouput: corners = (left, bottom, right, top)

    [ NB tiffs go from bottom to top so need to reverse the .y dimension]

    replicate rasterio's .bounds

    """

    return (
        input_ds.x.data[0],
        input_ds.y.data[-1],
        input_ds.x.data[-1],
        input_ds.y.data[0],
    )


# ==================
# prepare data for ingestion to tensorflow


# a function to create 2d data from 3d tiff
def flatten_tiff(inp, dim_keep=0):
    """
    inp_dims,reshaped_inp = flatten_tiff(inp,dim_keep=0)

    input: xr data_array
    output np.array

    flattens an input data array from 3d to 2d
    works if we want to squash the final dims and keep the first

    """

    inp_dims = inp.shape
    if (len(inp_dims)) == 2:
        inp = np.expand_dims(inp, axis=0)
        inp_dims = inp.shape
    if dim_keep == 0:
        reshaped_inp = np.reshape(inp, (inp_dims[0], inp_dims[1] * inp_dims[2]))

    return inp_dims, reshaped_inp


def select_bands_areas(dataset, corners, bands, subset_step=10):
    """
    ds_dims, ds_flat = select_bands_areas(dataset,corners,bands)

    inputs:
    dataset: input data (xarray, dataset)
    corners: co-ords of the edges
    bands: the bands (geotiff data so xarray has bands variable)
    subset_step, useful for picking out a subset of data for tractable tuning

    returns:
    ds_dims: the dimensions for un-flattening
    ds_flat: the flattened array


    """

    ds_subset = dataset.sel(
        x=slice(corners[0], corners[2], subset_step),
        y=slice(corners[3], corners[1], subset_step),
        band=bands,
    )

    ds_dims, ds_flat = flatten_tiff(ds_subset.band_data.data)

    return ds_dims, ds_flat
