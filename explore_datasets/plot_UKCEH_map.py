import rasterio
import numpy as np
import matplotlib.pyplot as plt
plt.ion()

#==========================
# open and plot the tiff from UKCEH

tiff = rasterio.open("data/ukregion-northeastengland.tif")


plt.figure()
x_plt = np.arange(tiff.xy(0,0)[0],tiff.xy(tiff.height,tiff.width)[0],10)
y_plt = np.arange(tiff.xy(0,0)[1],tiff.xy(tiff.height,tiff.width)[1],-10)

plt.pcolormesh(x_plt[::100],y_plt[::100],tiff.read()[0,::100,::100])
plt.clim(1,30)
plt.colorbar()

plt.show()


