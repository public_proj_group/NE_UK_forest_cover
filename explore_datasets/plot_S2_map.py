import rasterio
import numpy as np
import matplotlib.pyplot as plt

#===============

# open and plot the RGB channels of a sentinel 2 tiff

# open the tiff from EE
tiff = rasterio.open("data/2020_Jan_Mar-0000000000-0000000000.tif")
# tiff = rasterio.open("data/image_export_small.tif")


# get the visible bands
reds = tiff.read(4) # [:100,:100]
blues = tiff.read(2) # [:100,:100]
greens = tiff.read(3) # [:100,:100]

# get an RGB vector for plotting. 
rgb = np.dstack((reds,greens,blues))

# normalise the data for plotting
rgb/= 10000 # s2_sr values range from 0-10,000
rgb = np.clip(rgb,0,.25) # the range of reflectance should sit between 0 and .25
rgb /= .25 # get the values to be 0-1 for plotting


plt.figure()
x_plt = np.arange(tiff.xy(0,0)[0],tiff.xy(tiff.height,tiff.width)[0],10)
y_plt = np.arange(tiff.xy(0,0)[1],tiff.xy(tiff.height,tiff.width)[1],-10)

plt.pcolormesh(x_plt[::100],y_plt[::100],rgb[::100,::100])




