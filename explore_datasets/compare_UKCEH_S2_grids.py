import rasterio
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr

#===================================
# compare the grids from both UKCEH and Sentinel

# open the tiff from UKCEH
ukceh_tiff = rasterio.open("data/ukregion-northeastengland.tif")
ukceh_data = xr.open_dataset("data/ukregion-northeastengland.tif",engine="rasterio")

# open the tiff from S2
s2_tiff = rasterio.open("data/2020_Jan_Mar-0000000000-0000000000.tif")
s2_data = xr.open_dataset("data/2020_Jan_Mar-0000000000-0000000000.tif",engine="rasterio")

print(s2_tiff.bounds)
print(ukceh_tiff.bounds)

# find the co-ords of the corners for each data set
ukceh_corners = ukceh_tiff.bounds
s2_corners = s2_tiff.bounds 


# the UKCEH data covers more than the S2 so just get the bit of UKCEH that overlaps                 
ukceh_subset = ukceh_data.sel(x=slice(s2_corners[0],s2_corners[2]),y=slice(s2_corners[3],s2_corners[1]))

print (' getting subsets')
# at this point we can delete whole UKCEH data
del(ukceh_data)

# part of data sets where the land cover is 1 or 2
forest_s2_lc1 = s2_data.where(ukceh_subset.band_data[0,:,:] == 1)

# forest_s2_lc2 = s2_data.where(ukceh_subset.band_data[0,:,:] == 2)

